#!/usr/bin/env python3
# By Per Dahlstrøm
# V01: Linux VM on VMWW. Broker is mosquitto on LAN.
import paho.mqtt.client as mqtt
import random

brokerIP        = "192.168.0.64"
brokerPort      = 1883
brokerKeepAlive = 60

myTopic   = "Plant1/temperature"
myPayload = random.randint(0,99)
myQoS     = 1
myRetain  = True

client = mqtt.Client()
client.connect(brokerIP, brokerPort, brokerKeepAlive)
client.publish(topic =  myTopic, qos = myQoS, payload = myPayload, retain = myRetain);

print(str(myPayload) + " has been published on broker " + brokerIP + ":" + str(brokerPort) + " on topic: " + myTopic  )
client.disconnect();