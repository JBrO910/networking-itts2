# By Per Dahlstrøm
# V01: Linux VM on VMWW. Broker is mosquitto on LAN.
#!/usr/bin/env python3
import paho.mqtt.client as mqtt

brokerIP        = "192.168.0.64"
#brokerIP        = "broker.hivemq.com"
brokerPort      = 1883
brokerKeepAlive = 60
myTopic         = "Plant1/temperature"

def on_connect(client, userdata, flags, rc):
  # print("Connected with result code " + str(rc))
  client.subscribe(myTopic)

def message(client, userdata, msg):
    print("Fetched: " + str(msg.payload.decode()) + " from topic " + myTopic)
    client.disconnect()

client = mqtt.Client()
client.connect(brokerIP, brokerPort, brokerKeepAlive)
client.on_connect = on_connect
client.on_message = message
client.loop_forever()
